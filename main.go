package main
/*
https://github.com/wenerme/astgo/blob/master/ami/conn.go
 */
import (
	"fmt"
	"strconv"
	"github.com/wenerme/astgo/ami"
	"time"
	"encoding/json"
	"os"
	"bytes"
	"gopkg.in/olahol/melody.v1"
	"net/http"
	"github.com/gin-gonic/gin"
	"log"
	"io"
	"database/sql"
	_"github.com/lib/pq"
	_ "github.com/go-sql-driver/mysql"
)



var (
	err error
	db *sql.DB
	db_mysql *sql.DB
)



///структура для евентов фрисвича авторизации в колцентре
type eventAMI struct {
	event   string
	sip string
	head string
	value string
}



type error interface {
	Error() string
}


type wss struct {
	Event  string
}

type calls struct {
	toCall string
	fromCall string
	ch chan *ami.Command
	con ami.Conn
}


type Config struct {
	Asterisk struct {
		Host     string `json:"host"`
		Password string `json:"password"`
		Port string `json:"port"`
		User string `json:"user"`
	} `json:"asterisk"`
	Database struct {
		Host     string `json:"host"`
		Password string `json:"password"`
		DbName string `json:"dbName"`
		User string `json:"user"`
	} `json:"database"`
	DatabaseCDR struct {
		Host     string `json:"host"`
		Password string `json:"password"`
		DbName string `json:"dbName"`
		User string `json:"user"`
	} `json:"database_cdr"`
}

type dialAPI struct {
	Method string `form:"method"`
	Stete bool `form:"state"`
	ToCall string `form:"toCall"`
	FromCall string `form:"fromCall"`
	Login  string  `form:"login"`
	Password  string  `form:"password"`
	Name  string  `form:"name"`
	Number  string  `form:"number"`
	Book int `form:"book"`
	Channel string `form:"channel"`
	CdrAnum string `form:"cdrAnum"`
	CdrBnum string `form:"cdrBnum"`
}

func Asterisk(ch chan *ami.Command ,con ami.Conn, toCall string,fromCall string ,bufferChan chan calls)  {

	/*
	сначала звоним ToCall есили онивзял трубку то FromCall  потом соединяем
	 */

	 /*
	 Channel:  "sip/1000",
		Variable:"CallerID(1001)",
		CallerID: "1000",
		Exten:    "1001",
		Context:  "from-internal",
		Priority: "1",
		EarlyMedia:"false",
		//Application:"SIPAddHeader",
		////Data:"Call-Info: answer-after=0 ",
		Async:    "true",
	})
	  */




	res, err := con.WriteCommandSync(ami.OriginateAction{
		//		Channel:  "Local/89095876084@dialer", //номер на который звоним
		Channel:  "sip/"+toCall+"",
		//Channel:"Local/"+toCall+"@dialer",
		Variable:"Number="+toCall+"",
		CallerID: ""+fromCall+"",
		Exten:    ""+fromCall+"",
		Context:  "dialer",
		Priority: "1",
		EarlyMedia:"false",
		//Application:"SIPAddHeader",
		////Data:"Call-Info: answer-after=0 ",
		Async:    "true",
	})
	if err != nil {
		// Response Error
		//panic(err)
		log.Println("CRIT !!!!!!!!!!!!!!!!!!!!!!!!!!!!",err)
		eventCalls := calls{} //обьявили структуру  и записали туда данные
		eventCalls.toCall = toCall
		eventCalls.fromCall = fromCall
		eventCalls.ch = ch
		eventCalls.con = con

		//пишу данные в канал
		bufferChan <- eventCalls //пишем в канал структуру на очереди
		return
	}
	fmt.Println("Originate ", res.Message())
	// Will log some event
	<-time.After(time.Second * 30)
	//return
}

func cut(text string, limit int) string {
	runes := []rune(text)
	if len(runes) >= limit {
		return string(runes[:limit])
	}
	return text
}


//отправка данных с сокета астериска
func EventsScan (ch chan *ami.Command,con ami.Conn,channelAster chan eventAMI) {
	// Event handle
	for {
		c, ok := <-ch
		if !ok {
			//break
		}

		//fmt.Println(c)
		//	fmt.Println("!!!!!!!!!!!!",c.Event())
		//статус по вызову уже совершонному
		if c.Event() == "Cdr"  {
			log.Println("CDR Data  Calls  CallerID:BillableSeconds ",c.GetString("BillableSeconds"),"Durations",c.GetString("Duration"),"DestChannel",c.GetString("DestChannel"),"CallerID",c.GetString("CallerID"),"AnswerTime",c.GetString("AnswerTime"),"Start Time",c.GetString("StartTime"),"END Time",c.GetString("EndTime"),"UUID",c.GetString("Uniqueid"))
			//завершение вызова


			//&{Event map[AccountCode: CallerIDName:1001 CallerIDNum:1001 Cause:17 Cause-txt:User busy Channel:SIP/1001-000000c7 ChannelState:7 ChannelStateDesc:Busy ConnectedLineName:1000 ConnectedLineNum:1000 Context:ext-local Event:Hangup Exten:h Language:ru Linkedid:1566319804.199 Priority:1 Privilege:call,all Uniqueid:1566319804.199]}
			//!!!!!!!!!!!! Hangup
		}else if c.Event() == "Hangup" {
			fmt.Println("Edn Call ", c.GetString("CallerIDNum"))
			log.Println("CDR  Calls : " + c.Name(),"Cause TXT ",c.GetString("Cause-txt"),"CallerID",c.GetString("CallerIDNum"),"UUID",c.GetString("Uniqueid"),"Cause:",c.GetString("Cause"),"DestChannel",c.GetString("DestChannel"))
			//статус всех пиров которые есть на оборудовании

			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = c.Event()
			eventFSChan.head = c.GetString("CallerIDNum")
			eventFSChan.value = c.GetString("ConnectedLineNum")

			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди


		}else  if c.Event() == "PeerEntry" {
			fmt.Println("We are recieve status Peers", c.GetString("ObjectName"), "Status", c.GetString("Status"))

			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = "PeerStatus"
			eventFSChan.head = c.GetString("ObjectName")
			eventFSChan.value = c.GetString("Status")

			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди

			///пользователь ответил
			//&{Event map[AccountCode: CallerIDName:1001 CallerIDNum:1001 Channel:SIP/1001-000000b6 ChannelState:4 ChannelStateDesc:Ring ConnectedLineName:1000 ConnectedLineNum:1000 Context:macro-dial-one Event:VarSet Exten:s Language:ru Linkedid:1566319308.182 Priority:55 Privilege:dialplan,all Uniqueid:1566319308.182 Value:ANSWER Variable:DIALSTATUS]}
			//!!!!!!!!!!!! VarSet
		}else  if c.Event() == "VarSet" && c.GetString("Variable") == "DIALSTATUS"  {


			cut( c.GetString("Channel"), 8)
			substring := cut(c.GetString("Channel"), 8)[4:len(cut(c.GetString("Channel"), 8))]

			///&{Event map[AccountCode: CallerIDName:1000 CallerIDNum:1000 Channel:SIP/1001-0000080e ChannelState:6 ChannelStateDesc:Up ConnectedLineName:1000 ConnectedLineNum:1000 Context:macro-dial-one Event:VarSet Exten:s Language:ru Linkedid:1566460259.2062 Priority:55 Privilege:dialplan,all Uniqueid:1566460259.2062 Value:ANSWER Variable:DIALSTATUS]}
			    fmt.Println(c)
				fmt.Println("!!!!!!!!!!!!",c.Event())

			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = c.GetString("Value")
			eventFSChan.head = substring  // куда звонят
			eventFSChan.value = c.GetString("CallerIDNum") //кто звонит
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди


		}else if c.Event() == "VarSet" && c.GetString("Value") == "ANSWER"{

			fmt.Println(c)
			fmt.Println("!!!!!!!!!!!!",c.Event())


			cut( c.GetString("Channel"), 8)
			substring := cut(c.GetString("Channel"), 8)[4:len(cut(c.GetString("Channel"), 8))]




			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = c.GetString("Value")
			eventFSChan.head = substring  // куда звонят
			eventFSChan.value = c.GetString("CallerIDNum") //кто звонит
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди

			//делает вызов
		}else if c.Event()=="Newchannel" {
			fmt.Println("call from", c.GetString("CallerIDNum"), "To", c.GetString("CallerIDNum"))
			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = "NewChannel"
			eventFSChan.head = c.GetString("CallerIDNum")
			eventFSChan.value = c.GetString("CallerIDNum")
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди

		}else if c.Event()=="DeviceStateChange"{
			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = c.GetString("State")
			eventFSChan.head = c.GetString("Device")
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди

		}else if  c.Event() == "Newexten"{
			///&{Event map[Address:91.210.4.8:20706 ChannelType:SIP Event:PeerStatus Peer:SIP/1001 PeerStatus:Registered Privilege:system,all]}
			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = c.GetString("State")
			eventFSChan.head = c.GetString("CallerIDNum")
			eventFSChan.value = c.GetString("Exten")
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди

			////зарегистрировались
		}else if c.Event() == "PeerStatus" {

			///&{Event map[Address:91.210.4.8:20706 ChannelType:SIP Event:PeerStatus Peer:SIP/1001 PeerStatus:Registered Privilege:system,all]}
			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = c.GetString("PeerStatus")
			eventFSChan.head = c.GetString("Peer")
			//eventFSChan.value = c.GetString("PeerStatus")
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди

			//отдаем в сокет список каналов
		}else if c.Event()=="CoreShowChannel"{

			/*
			!!!!!!!!!!!! CoreShowChannel
			&{Event map[AccountCode: ActionID:10 Application:AppDial ApplicationData:(Outgoing Line) BridgeId:51dceaf9-d582-4af3-a583-482e48431d71 CallerIDName:1001 CallerIDNum:1001 Channel:SIP/1001-00000675 ChannelState:6 ChannelStateDesc:Up ConnectedLineName:1000 ConnectedLineNum:1000 Context:from-internal Duration:00:00:42 Event:CoreShowChannel Exten: Language:ru Linkedid:1566408916.1652 Priority:1 Uniqueid:1566408916.1653]}
			 */
			eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
			eventFSChan.event = "CoreShowChannel"
			eventFSChan.head = c.GetString("Channel")
			eventFSChan.value = c.GetString("CallerIDNum")
			//пишу данные в канал
			channelAster <- eventFSChan //пишем в канал структуру на очереди
		}
	}
}








func LoadConfiguration(file string) Config {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config
}

func welcome(c *gin.Context) {
	var b dialAPI
	c.Bind(&b)
	c.JSON(
		http.StatusOK,
		gin.H{
			"messages": "API is working !!!!! Hello in the voicecalls portal  Rest API",
		},
	)
}

func responseJSON(c *gin.Context,text string,state string)  {
	c.JSON(
		http.StatusOK,
		gin.H{
			state: text ,
		},
	)
}

func readBuffer(msg chan calls,ch chan *ami.Command ) {
	for {
		time.Sleep(5 * time.Millisecond)
		select {
		case channelAuth := <-msg:
			log.Println("CRIT !!!!!!!!!!!!!!!!!!!!!!!!!!!! Repeat Connetc ",channelAuth)
			go  newConnections(ch,msg,channelAuth)
		default:
			//fmt.Println("no message received")
		}
	}
}

func newConnections(ch chan *ami.Command,bufferChan chan calls ,channelAuth calls)  {
	mainConfig := LoadConfiguration("./config.json")
	//с канала читаем ошибочные конннекты и отправляем их на переповтор
	con, err := ami.Dial(""+mainConfig.Asterisk.Host+":"+mainConfig.Asterisk.Port+"", ami.DialConf{
		Username:  ""+mainConfig.Asterisk.User+"",
		Secret:    ""+mainConfig.Asterisk.Password+"",
		Reconnect: true,
		Debug:true,
		Listeners: []chan<- *ami.Command{ch},
	})
	if err != nil {
		panic(err)
	}
	go  Asterisk( channelAuth.ch , con ,channelAuth.toCall,channelAuth.fromCall,bufferChan)
	defer con.Close()
}


func createKeyValuePairs(m map[string]string) string {
	b := new(bytes.Buffer)
	for key, value := range m {
		fmt.Fprintf(b, "%s=\"%s\"\n", key, value)
	}
	return b.String()
}

func redirectSIP(con ami.Conn,channnel string,exten string )  {
	con.WriteCommandSync(ami.RedirectAction{
		Channel:""+channnel+"",
		Exten :""+exten+"",
		Context:  "from-internal",
	})

	//Channel:  "sip/"+toCall+"",
	//		CallerID: ""+fromCall+"",
	//		Exten:    ""+fromCall+"",
	//		Context:  "from-internal",
	//		Priority: "1",
	//		EarlyMedia:"false",
}

//отправляю команду для вывода всех сип каналов на АТС
func holdSIP(con ami.Conn)  {
	con.WriteCommandSync(ami.CoreShowChannelsAction{}) //ждем вывод в сокете

	/*
	&{Event map[AccountCode: ActionID:7 Application:Dial ApplicationData:SIP/1001,,HhTtrIb(func-apply-sipheaders^s^1) BridgeId:c5c55b07-2f38-44e7-9959-5f509bc4fea5 CallerIDName:1000 CallerIDNum:1000 Channel:SIP/1000-0000065c ChannelState:6 ChannelStateDesc:Up ConnectedLineName:1001 ConnectedLineNum:1001 Context:macro-dial-one Duration:00:00:05 Event:CoreShowChannel Exten:s Language:ru Linkedid:1566407671.1628 Priority:55 Uniqueid:1566407671.1628]}
	!!!!!!!!!!!! CoreShowChannel
	&{Event map[AccountCode: ActionID:7 Application:AppDial ApplicationData:(Outgoing Line) BridgeId:c5c55b07-2f38-44e7-9959-5f509bc4fea5 CallerIDName:1001 CallerIDNum:1001 Channel:SIP/1001-0000065d ChannelState:6 ChannelStateDesc:Up ConnectedLineName:1000 ConnectedLineNum:1000 Context:from-internal Duration:00:00:05 Event:CoreShowChannel Exten: Language:ru Linkedid:1566407671.1628 Priority:1 Uniqueid:1566407672.1629]}
	!!!!!!!!!!!! CoreShowChannel
	&{Event map[ActionID:7 Event:CoreShowChannelsComplete EventList:Complete ListItems:2]}
	!!!!!!!!!!!! CoreShowChannelsComplete
	 */
}

func hangupSIP(con ami.Conn ,channnel string )()  {
	con.WriteCommandSync(ami.HangupAction{
		Channel:""+channnel+"",
	})
}

func holdSIPactions(con ami.Conn ,channnel string )()  {
	con.WriteCommandSync(ami.ParkAction{
		Channel:""+channnel+"",
		Timeout: "20",
	})
}

//получаю список всех зарегистрированных юзеров на АТС
func listPeers(ch chan *ami.Command,con ami.Conn)  {
	con.WriteCommandSync(ami.SIPpeersAction{
	})
	//отправили команду и далее получаем в эвентах событие о том что изменилось
}

func setDND(login string ,state bool ,channelAster chan eventAMI,con ami.Conn)  {
	_, err = db.Exec("update contacts SET dnd= $1 where number = $2  ",state,login)


	if state == true {
		con.WriteCommandSync(ami.OriginateAction{
			//		Channel:  "Local/89095876084@dialer", //номер на который звоним
			Channel:  "sip/"+login+"",
			//Channel:"Local/"+toCall+"@dialer",
			//Variable:"Number="+toCall+"",
			CallerID: ""+login+"",
			Exten:    "*78",
			Context:  "dnd_enable",
			Priority: "1",
			EarlyMedia:"false",
			//Application:"SIPAddHeader",
			////Data:"Call-Info: answer-after=0 ",
			Async:    "true",
		})
	}else {
		con.WriteCommandSync(ami.OriginateAction{
			//		Channel:  "Local/89095876084@dialer", //номер на который звоним
			Channel:  "sip/"+login+"",
			//Channel:"Local/"+toCall+"@dialer",
			//Variable:"Number="+toCall+"",
			CallerID: ""+login+"",
			Exten:    "*79",
			Context:  "dnd_disable",
			Priority: "1",
			EarlyMedia:"false",
			//Application:"SIPAddHeader",
			////Data:"Call-Info: answer-after=0 ",
			Async:    "true",
		})

	}




	checkErr(err)
	//нотифицируем всем в канал
	eventFSChan := eventAMI{} //обьявили структуру  и записали туда данные
	eventFSChan.event = "DND"
	eventFSChan.head = strconv.FormatBool(state)
	eventFSChan.value = login
	//пишу данные в канал
	channelAster <- eventFSChan //пишем в канал структуру на очереди
}



func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}


func checkUser(login string,password string  ) bool {
	var  count  bool
	data_rows , err := db.Query("select exists(SELECT * FROM users WHERE login = $1 AND password = $2) as count ",login,password)
	checkErr(err)  ///функция обработки ошибок
	defer data_rows.Close()  ///закрываем коннект к Базе данных
	for data_rows.Next() {
		data_rows.Scan(&count)
	}
	return count
}

//функция которая формирует список для WSS котактов на вход получаем толькко логин
func getToWSS(str string ) string {

	//str:= "/channel/1000/ws"
	substring := cut(str, 13)[9:len(cut(str, 13))]


	//формируем контакты
	conf,_ := getContacts(substring)
	//var data = make(map[string][]alive)
	//фомируем фаворитов
	user,_:= userCC(substring)
	favorit,_:= getFavorites(substring)

	b := map[string]interface{}{
		"type":   "book_contact",
		"contacts": conf,
		"user":   user,
		"favorite":   favorit,
	}

	// Marshal the map into a JSON string.
	empData, _ := json.Marshal(b)
	jsonStr := string(empData)
	fmt.Println("The JSON data is:")
	fmt.Println(jsonStr)

	return jsonStr
}

func updateState(status string ,login string )  {
	_, err = db.Exec("update contacts SET status= $1 where number = $2  ",status,login)
	checkErr(err)
}

func setupRouter(ch chan *ami.Command,con ami.Conn,bufferChan chan calls,m *melody.Melody,channelAster chan eventAMI) *gin.Engine {
	f, err := os.OpenFile("./rest.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	r.GET("/", welcome)
	///API на обзвон
	gin.SetMode(gin.DebugMode)
	gin.DefaultWriter = io.MultiWriter(f)
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	gin.DefaultWriter = io.MultiWriter(f) // display nothing, but still write the file
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout) // display logs and write the file
	log.SetOutput(gin.DefaultWriter) // You may need this

	//websocket
	r.GET("/ws", func(c *gin.Context) {
		m.HandleRequest(c.Writer, c.Request)
	})


	r.GET("/channel/:name", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, "chan.html")
	})

	r.GET("/channel/:name/ws", func(c *gin.Context) {
		m.HandleRequest(c.Writer, c.Request)
	})


	//getData := getToWSS(s.Request.URL.Path)
	m.HandleMessage(func(s *melody.Session, msg []byte) {
		//fmt.Println("get",msg)
		m.BroadcastFilter([]byte(getToWSS(s.Request.URL.Path)), func(q *melody.Session) bool {
			fmt.Println("Login",q.Request.URL.Path)
			return q.Request.URL.Path == s.Request.URL.Path
		})

	})

	//читаем из канала и отправяем в веб сокет
	go func() {
		for {
			c, ok := <-channelAster
			if !ok {
				//break
			}
			fmt.Println(c)
			/*
			кидаем данные в мапу потом ее же переводим в строку
			 */

			//Обработка событий регистрация /оффлайн
			if c.event == "Registered" || c.event == "Unregistered" {
				/*
				 VALUE="SIP/1001"
                  EVENT="Unreachable"
				  */

				substring := c.head[4:len(c.head)]

				//забираем данные о клиенте с базы

				data, _ := userSIP(substring)
				id := strconv.Itoa(data.Id)

				b := map[string]string{
					"type":   "update_contact",
					"status": "" + c.event + "",
					"line":   "" + substring + "",
					"name":   data.Name,
					"id":     id,
				}
				// Marshal the map into a JSON string.
				empData, err := json.Marshal(b)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				jsonStr := string(empData)
				fmt.Println("The JSON data is:")
				fmt.Println(jsonStr)
				go  updateState(c.event ,substring )
				m.Broadcast([]byte((jsonStr)))

			}else if c.event ==  "RINGING" && c.value != "s" && c.value !="h"  {
				substring := c.head[4:len(c.head)]
				data, _ := userSIP(substring)
				id := strconv.Itoa(data.Id)
				b := map[string]string{
					"type":   "update_contact",
					"status": "" + c.event + "",
					"line":   "" + substring + "",
					"toCall":   "" + c.value+ "",
					"name":   data.Name,
					"id":     id,
				}
				// Marshal the map into a JSON string.
				empData, err := json.Marshal(b)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				jsonStr := string(empData)
				fmt.Println("The JSON data is:")
				fmt.Println(jsonStr)
				go  updateState(c.event ,substring )
				m.Broadcast([]byte((jsonStr)))
			}else if c.event == "ANSWER"{
				data, _ := userSIP(c.head)
				id := strconv.Itoa(data.Id)
				b := map[string]string{
					"type":   "update_contact",
					"status": "" + c.event + "",
					"line":   "" + c.head + "",
					"toCall":   "" + c.value+ "",
					"name":   data.Name,
					"id":     id,
				}
				// Marshal the map into a JSON string.
				empData, err := json.Marshal(b)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				jsonStr := string(empData)
				fmt.Println("The JSON data is:")
				go  updateState(c.event ,c.head )
				fmt.Println(jsonStr)
				m.Broadcast([]byte((jsonStr)))
			}else if c.event == "Hangup"{
				data, _ := userSIP(c.head)
				id := strconv.Itoa(data.Id)
				b := map[string]string{
					"type":   "update_contact",
					"status": "" + c.event + "",
					"line":   "" + c.head + "",
					"toCall":   "" + c.value+ "",
					"name":   data.Name,
					"id":     id,
				}
				// Marshal the map into a JSON string.
				empData, err := json.Marshal(b)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				jsonStr := string(empData)
				fmt.Println("The JSON data is:")
				go  updateState(c.event ,c.head )
				fmt.Println(jsonStr)
				m.Broadcast([]byte((jsonStr)))
			}else if c.event== "DND" {
				data, _ := userSIP(c.value)
				id := strconv.Itoa(data.Id)
				b := map[string]string{
					"type":   "update_contact",
					"status": "" + c.event + "",
					"line":   "" + c.value+ "",
					"state":"" + c.head+ "",
					"name":   data.Name,
					"id":     id,
				}
				//{"id":"0","line":"false","name":"","status":"DND","toCall":"1000","type":"update_contact"}
				// Marshal the map into a JSON string.
				empData, err := json.Marshal(b)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				jsonStr := string(empData)
				fmt.Println("The JSON data is:")
				fmt.Println(jsonStr)
				m.Broadcast([]byte((jsonStr)))
			}else if c.event =="CoreShowChannel"{
				data, _ := userSIP(c.value)
				id := strconv.Itoa(data.Id)
				b := map[string]string{
					"type":   "update_contact",
					"status": "" + c.event + "",
					"line":   "" + c.value+ "",
					"state":"" + c.head+ "",
					"name":   data.Name,
					"id":     id,
				}
				//{"id":"0","line":"false","name":"","status":"DND","toCall":"1000","type":"update_contact"}
				// Marshal the map into a JSON string.
				empData, err := json.Marshal(b)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				jsonStr := string(empData)
				fmt.Println("The JSON data is:")
				fmt.Println(jsonStr)
				m.Broadcast([]byte((jsonStr)))
			}
 		}
	}()


	r.GET("/api", func(c *gin.Context) {
		var apiDial dialAPI
		c.Bind(&apiDial)
		fmt.Println(apiDial.Method)
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		if apiDial.Method == "dialing" {
			go  Asterisk(ch ,con ,apiDial.ToCall,apiDial.FromCall,bufferChan)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status":  "ok wait CDR",
				},
			)
			return
		}else if apiDial.Method ==  "getAllstatusSIP" {
			go listPeers(ch, con)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok wait CDR ",
				},
			)
			return
		}else if apiDial.Method ==  "setDND" {
			go setDND(apiDial.Login ,apiDial.Stete ,channelAster,con)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok",
				},
			)
			return
		}else if apiDial.Method ==  "channelSIP" {
			holdSIP(con)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok",
				},
			)
			return
		}else if apiDial.Method ==  "channelSIPhangup" {
			hangupSIP(con,apiDial.Channel)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok",
				},
			)
			return
		}else if apiDial.Method ==  "channelSIPHold" {
			holdSIPactions(con,apiDial.Channel)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok",
				},
			)
			return

			/// redirectSIP(con ami.Conn,channnel string,exten string )
		}else if apiDial.Method ==  "channelSIPredirect" {
			redirectSIP(con,apiDial.Channel,apiDial.Number )
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok",
				},
			)
			return
		}else if apiDial.Method == "addUser"{
				_, err = db.Exec("insert into users (name, login, password, number,book)  values ($1,$2,$3,$4,$5) ",apiDial.Name,apiDial.Login,apiDial.Password,apiDial.Number,apiDial.Book)
				checkErr(err)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok",
				},
			)
			return
		}else if apiDial.Method == "addUserToBook"{
			_, err = db.Exec("insert into contacts (name,number,book)  values ($1,$2,$3) ",apiDial.Name,apiDial.Number,apiDial.Book)
			checkErr(err)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok  ",
				},
			)
			return

		}else if apiDial.Method == "addFavorite"{
			_, err = db.Exec("insert into favorites  (user_id,name,number)  values ($1,$2,$3) ",apiDial.Login,apiDial.Name,apiDial.Number)
			checkErr(err)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok  ",
				},
			)
			return
		}else if apiDial.Method == "deleteFavorite"{
			_, err = db.Exec("delete  from  favorites  where user_id = $1 and number = $2 ",apiDial.Login,apiDial.Number)
			checkErr(err)
			c.JSON(
				http.StatusOK,
				gin.H{
					"status": "ok  ",
				},
			)
			return
		}else if apiDial.Method == "auth"{
			if checkUser(apiDial.Login,apiDial.Password) == true {

				//формируем контакты
				conf,_ := getContacts(apiDial.Login)
				//var data = make(map[string][]alive)
				//фомируем фаворитов
				user,_:= userCC(apiDial.Login)

                favorit,_:= getFavorites(apiDial.Login)
				//var data = make(map[string][]alive)


				//				select name,number from contacts where book =  (select book from users where login = '1000');
				c.JSON(
						http.StatusOK,
						gin.H{
							"user":user,
							"contacts":  conf,
							"favorites":favorit,
						},
					)
					return
				}else {
				c.JSON(
					http.StatusOK,
					gin.H{
						"status":"error",
						"message": "wrong_pass",
					},
				)
			return
			}
		}else if apiDial.Method == "cdr"{

			//CdrAnum string `form:"cdrAnum"`
			//	CdrBnum string `form:"cdrBnum"`
			

				conf:= getCDR(apiDial.CdrAnum,apiDial.CdrBnum)
				c.JSON(
					http.StatusOK,
					gin.H{
						"cdr":conf,
					},
				)
				return
		}else {
			responseJSON(c,"method not defined !!!","error")
		}
	})


	go r.Run(":8090")
	return r
}


type user  struct {
	Id  int          `json:"id"`
	Name	string           `json:"name"`
	Number string     `json:"number"`
	Book int  `json:"book"`
}



func userSIP(number string ) (user,error) {
	var statCC user
	var rows *sql.Rows
	//var rs= make([]cdr, 0)

	rows, err = db.Query("select id,name,number,book from users where  number = $1; ",number)
	checkErr(err)
	defer rows.Close()
	for rows.Next() {
		if err = rows.Scan(&statCC.Id,&statCC.Name,&statCC.Number,&statCC.Book);
			err != nil {
			fmt.Println(err)
			return statCC , err
		}
		//rs = append(rs,statCC)
	}
	if err = rows.Err(); err != nil {
		return statCC , err
	}
	return statCC , nil
}


func userCC(login string ) (user,error) {
	var statCC user
	var rows *sql.Rows
	//var rs= make([]cdr, 0)


		rows, err = db.Query(" select id,name,number,book from users where  login = $1; ",login)
		checkErr(err)
		defer rows.Close()
		for rows.Next() {
			if err = rows.Scan(&statCC.Id,&statCC.Name,&statCC.Number,&statCC.Book);
				err != nil {
				fmt.Println(err)
				return statCC , err
			}
			//rs = append(rs,statCC)
		}
		if err = rows.Err(); err != nil {
			return statCC , err
		}
	return statCC , nil
}



type alive struct {
	Id	int           `json:"id"`
	Name	string           `json:"name"`
	Number string     `json:"number"`
	Status string `json:"status"`
	Dnd bool `json:"dnd"`
}

type cdr struct {
	Calldate	time.Time       `json:"calldate"`
    Src string `json:"src"`
	Dst string  `json:"dst"`
	Duration int   `json:"duration"`
	Billsec  sql.NullInt64 `json:"bilsec"`
	Disposition string `json:"disposition
"`

}
//calldate,src,dst,duration,billsec,disposition


func getCDR(cdrNumA string,cdrNumB string) ([]cdr)  {

	mainConfigCDR := LoadConfiguration("./config.json")


	db_mysql, _ = sql.Open("mysql", ""+mainConfigCDR.DatabaseCDR.User+":"+mainConfigCDR.DatabaseCDR.Password+"@("+mainConfigCDR.DatabaseCDR.Host+":3306)/"+mainConfigCDR.DatabaseCDR.DbName+"?parseTime=true")
	defer db_mysql.Close()
	var version string
	db_mysql.QueryRow("SELECT VERSION()").Scan(&version)
	fmt.Println("Connected to:", version)
	// проверяем что подключение реально произошло ( делаем запрос )
	err = db_mysql.Ping()
	checkErr(err) ///проверяю коннект
	defer db_mysql.Close()
	//fmt.Println("CONNECTED TO POSTGRES DATABASE")

	var bank cdr
	var directory_user = make([]cdr, 0)
	//data_company, err := db_mysql.Query("select calldate,src,dst,duration,billsec,disposition from cdr where  dst = ? or src = ? and dst !='s' order by calldate  DESC ",cdrNumA,cdrNumA)
	data_company, err := db_mysql.Query("SELECT MIN(calldate) AS calldate,src,dst,duration,billsec,disposition FROM cdr WHERE (src = ?  OR dst = ? ) AND dst != 's' AND dst != 's' GROUP BY linkedid ORDER BY calldate DESC ",cdrNumA,cdrNumA)
	checkErr(err)
	defer data_company.Close()
	for data_company.Next() {
		if err = data_company.Scan(&bank.Calldate,&bank.Src,&bank.Dst,&bank.Duration,&bank.Billsec,&bank.Disposition);
			err != nil {
			fmt.Println("DB ERR",err)
		}
		////делаем слайс стрингов и добавляем в него значения юзеров которые есть
		directory_user = append(directory_user, bank)
	}
	return directory_user
}

func getContacts(login string) ([]alive,error)  {
	var rows *sql.Rows
	var bank alive
	rows, err = db.Query("select id,name,number,status,dnd from contacts where book =  (select book from users where login = $1) order by id  ",login)
	checkErr(err)
	defer rows.Close()
	var rs = make([]alive, 0)
	for rows.Next() {
		if err = rows.Scan(&bank.Id,&bank.Name,&bank.Number,&bank.Status,&bank.Dnd);
			err != nil {
			return rs , err
		}
		rs = append(rs, bank)
	}
	if err = rows.Err(); err != nil {
		return rs , err
	}
	return rs , nil
}

func getFavorites(login string) ([]alive,error)  {
	var rows *sql.Rows
	var bank alive
	rows, err = db.Query("select id,name,number from favorites where user_id = $1 order by id ",login)
	checkErr(err)
	defer rows.Close()
	var rs = make([]alive, 0)
	for rows.Next() {
		if err = rows.Scan(&bank.Id,&bank.Name,&bank.Number,);
			err != nil {
			return rs , err
		}
		rs = append(rs, bank)
	}
	if err = rows.Err(); err != nil {
		return rs , err
	}
	return rs , nil
}



func main() {
	mainConfig := LoadConfiguration("./config.json")
	db_property := "host=" + mainConfig.Database.Host + " user=" + mainConfig.Database.User + " password=" + mainConfig.Database.Password + " dbname=" + mainConfig.Database.DbName + " sslmode=disable"
	db, err = sql.Open("postgres", db_property)
	err = db.Ping()
	defer db.Close()
	if err != nil {log.Println("DATABASE IS NOT CONNECTED  " + mainConfig.Database.DbName + " Host: " + mainConfig.Database.Host)
	} else {
	ch := make(chan *ami.Command, 1024)

	//создаем канал для запрсов которые ушли по реконекту чтобы не рушить очередь
	bufferChan := make(chan calls, 1024)

	channelAster := make(chan eventAMI,1024)


	//с канала читаем ошибочные конннекты и отправляем их на переповтор
	go readBuffer(bufferChan,ch)

	con, err := ami.Dial(""+mainConfig.Asterisk.Host+":"+mainConfig.Asterisk.Port+"", ami.DialConf{
		Username:  ""+mainConfig.Asterisk.User+"",
		Secret:    ""+mainConfig.Asterisk.Password+"",
		Reconnect: true,
		Debug:true,
		Listeners: []chan<- *ami.Command{ch},
	})
	if err != nil {
		panic(err)
	}

	m := melody.New()


	fmt.Println("we are connet to asterisk")


	gin.SetMode(gin.DebugMode)
	log.SetOutput(gin.DefaultWriter) // You may need this
	//r := gin.Default()

	setupRouter(ch,con,bufferChan,m,channelAster)

	//Read Events from Asterisk
	EventsScan(ch,con,channelAster)
}}
